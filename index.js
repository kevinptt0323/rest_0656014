import request from 'superagent'
import keystone from './keystone';
import glance from './glance';
import compute from './compute';

const config = require('./config.json');

const sleep = timeout => new Promise(resolve => {
  setTimeout(resolve, timeout);
});

(async () => {
  if (process.argv.length<3) {
    console.log(`\x1b[31mError: please specify instance name\x1b[m`);
    return;
  }
  const name = process.argv[2];
  const token = await keystone.authToken(config);
  console.log('\x1b[32mlogin usccess!\x1b[m');
  console.log(`token: ${token}`);

  const images = await glance.images(config, token);
  const cirros = images.filter(image => image.name.includes('cirros'))[0];

  const flavors = await compute.flavors(config, token);
  const m1_nano = flavors.filter(flavor => flavor.name.includes('m1.nano'))[0];

  let res = await compute.createServer(config, token, name, cirros.id, m1_nano.id);
  const serverId = res.id;
  console.log(`\x1b[32mcreate instance "${name}" success!\x1b[m`);
  console.log(`instance id: ${serverId}`);

  let addresses = false;
  while(!addresses) {
    const servers = await compute.servers(config, token);
    const server = servers.filter(server => server.id == serverId)[0];
    if (server.addresses.public) {
      addresses = server.addresses;
    } else {
      await sleep(1000);
    }
  }
  console.log('\x1b[32mget instance public address:\x1b[m');
  addresses.public.forEach(address => {
    console.log(`${address.addr}`)
  });

})();



