import request from 'superagent';

export const flavors = (config, token) => {
  return request
    .get(`${config.computeUrl}/v2/flavors`)
    .set('X-Auth-Token', token)
    .then(res => res.body)
    .then(data => data.flavors);
};

export const servers = (config, token) => {
  return request
    .get(`${config.computeUrl}/v2/servers/detail`)
    .set('X-Auth-Token', token)
    .then(res => res.body)
    .then(data => data.servers);
};

export const createServer = (config, token, name, imageId, flavorId) => {
  const body = {
    server: {
      name,
      imageRef: imageId,
      flavorRef: flavorId,
    }
  };
  return request
    .post(`${config.computeUrl}/v2/servers`)
    .set('X-Auth-Token', token)
    .send(body)
    .then(res => res.body)
    .then(data => data.server);
};

export default {
  flavors,
  servers,
  createServer,
};
