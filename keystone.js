import request from 'superagent';

export const authToken = (config) => {
  const body = {
    'auth': {
      'identity': {
        'methods': ['password'],
        'password': {
          'user': {
            'name': config.username,
            'domain': {
              'name': config.domain
            },
            'password': config.password
          }
        }
      },
      'scope': {
        'project': {
          'name': config.username,
          'domain': {
            'name': config.domain
          }
        }
      }
    }
  };


  return request
    .post(`${config.keystoneUrl}/v3/auth/tokens`)
    .send(body)
    .then(res => res.header['x-subject-token']);
};

export default {
  authToken,
};
