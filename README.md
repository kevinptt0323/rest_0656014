# Homework 3-1

## Requirement
- node v6+
- npm v3+ or yarn v1

## Configuration

Edit `config.json`.

Change `username` and `password`.

Change `keystoneUrl`, `glanceUrl`, `computeUrl` to the url of keystone, glance
and compute services alternatively.

```
{
  "domain": "Default",
  "username": "admin",
  "password": "password",
  "keystoneUrl": "http://localhost/identity",
  "glanceUrl": "http://localhost/image",
  "computeUrl": "http://localhost/compute"
}
```

## Usage

### npm
```
npm install
npm start <instance_name>
```

### yarn
```
yarn install
yarn start <instance_name>
```
