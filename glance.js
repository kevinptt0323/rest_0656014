import request from 'superagent';

export const images = (config, token) => {
  return request
    .get(`${config.glanceUrl}/v2/images`)
    .set('X-Auth-Token', token)
    .then(res => res.body)
    .then(data => data.images);
};

export default {
  images,
};
